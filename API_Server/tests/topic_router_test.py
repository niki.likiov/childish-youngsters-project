from datetime import datetime
import unittest
from unittest.mock import Mock

from common.responses import NotFound
from data.models import Topic

from routers import topics as topics_router

mock_topic_service = Mock(spec="services.topics_service")
topics_router.topics_service = mock_topic_service
mock_auth = Mock(spec="common.auth")
topics_router.get_user_or_raise_401 = mock_auth


def fake_topic(
    id=1,
    title="Mock title here",
    is_locked=1,
    created_on=2002,
    author_id=3,
    category_id=2,
    best_reply_id=4,
):
    mock_topic = Mock(spec=Topic)
    mock_topic.id = id
    mock_topic.title = title
    mock_topic.is_locked = is_locked
    mock_topic.created_on = created_on
    mock_topic.author_id = author_id
    mock_topic.category_id = category_id
    mock_topic.best_reply_id = best_reply_id
    return mock_topic


class TopicRouter_Should(unittest.TestCase):
    def setUp(self) -> None:
        mock_topic_service.reset_mock()

    def test_createTopic(self):
        topic = fake_topic()
        mock_topic_service.create = lambda t, author_id: topic
        mock_topic_service.find_by_id = lambda id: topic
        result = topics_router.new_topic(topic)
        expected = topics_router.get_topic_by_id(result.id)

        self.assertEqual(expected, result)

    def test_getTopicById_returns_Topic(self):
        topic = Topic(
            id=1,
            title="Пране",
            is_locked=True,
            created_on=datetime(2022, 2, 2, 2, 2, 2),
            author_id=2,
            category_id=3,
        )
        mock_topic_service.find_by_id = lambda id: topic
        result = topics_router.get_topic_by_id(topic.id)
        expected = topic
        self.assertEqual(expected, result)

    def test_getTopicById_returns_NotFound_when_noTopic(self):
        mock_topic_service.find_by_id = lambda id: None
        result = type(topics_router.get_topic_by_id(1))
        expected = type(NotFound())
        self.assertEqual(expected, result)
