from mariadb import IntegrityError
import unittest
from datetime import datetime
from unittest.mock import Mock
from data.models import Category, Topic
from services import category_service
from tests.user_service_test import fake_user


def fake_category(id=1, name="PeshoStuff", is_private=0, is_locked=0):
    mock_category = Mock(spec=Category)
    mock_category.id = id
    mock_category.name = name
    mock_category.is_private = is_private
    mock_category.is_locked = is_locked
    return mock_category


class CategoryService_Should(unittest.TestCase):
    def test_getAllForGuest_returnsListOfCategories(self):
        get_data_func = lambda q: [(1, "TV", 1, 0), (2, "Computers", 0, 0)]
        result = list(category_service.all_for_guest(get_data_func))
        self.assertEqual(2, len(result))

    def test_getAllForUser_returnsListOfCategories(self):
        user = fake_user()
        get_data_func = lambda q, params: [(1, "TVv", 0, 0), (2, "Computers", 0, 0)]
        result = list(category_service.all_for_user(user.id, get_data_func))
        self.assertEqual(2, len(result))

    def test_getAllForAdmin_returnsListOfCategories(self):
        get_data_func = lambda q: [(1, "TVv", 0, 0), (2, "Computers", 0, 0)]
        result = list(category_service.all_for_admin(get_data_func))
        self.assertEqual(2, len(result))

    def test_getById_returnsCategory(self):
        get_data_func = lambda q, category_id: [(1, "Phones", 0, 0)]
        result = category_service.get_by_id(5, get_data_func)
        expected = Category(id=1, name="Phones", is_private=0, is_locked=0)
        self.assertEqual(result, expected)

    def test_getById_returnsNoneOnWrongID(self):
        category = fake_category()
        get_data_func = lambda q, category_id: []
        result = category_service.get_by_id(category.id, get_data_func)
        expected = None
        self.assertEqual(result, expected)

    def test_getTopics_returnsListOfTopics(self):
        category = fake_category()
        get_data_func = lambda q, topic_id: [
            (1, "title", 0, datetime(2022, 2, 2, 10, 10, 10), 5, 6),
            (2, "title2", 0, datetime(2022, 2, 2, 10, 10, 10), 5, 8),
        ]
        result = list(category_service.get_topics(category.id, get_data_func))
        expected = [
            Topic(
                id=1,
                title="title",
                is_locked=0,
                created_on=datetime(2022, 2, 2, 10, 10, 10),
                author_id=5,
                category_id=6,
            ),
            Topic(
                id=2,
                title="title2",
                is_locked=0,
                created_on=datetime(2022, 2, 2, 10, 10, 10),
                author_id=5,
                category_id=8,
            ),
        ]
        self.assertEqual(result, expected)

    def test_Exists_returnsTrue_OnExistentCategory(self):
        category = fake_category()
        get_data_func = lambda q, category_id: [True]
        result = category_service.exists(category.id, get_data_func)
        expected = True
        self.assertEqual(result, expected)

    def test_Exists_returnsFalse_OnNonExistentCategory(self):
        category = fake_category()
        get_data_func = lambda q, category_id: [False]
        result = category_service.exists(category.id, get_data_func)
        expected = False
        self.assertEqual(result, expected)

    def test_Create_returnsCategory_OnCorrectName(self):
        category = fake_category()
        get_data_func = lambda q, category_id: category.id
        result = category_service.create(category.name, get_data_func)
        expected = Category(id=1, name="PeshoStuff", is_private=0, is_locked=0)
        self.assertEqual(result, expected)

    def test_Create_throwsIntegrityError_OnDuplicateName(self):
        def get_data_func(name, q):
            raise IntegrityError("Duplicate category name")

        category = fake_category()
        result = category_service.create(category.name, get_data_func)
        expected = "Error: Duplicate category name"
        self.assertEqual(result, expected)

    def test_Lock_returnsLockedCategory(self):
        category = Category(id=1, name="PeshoStuff", is_private=0, is_locked=0)
        get_data_func = lambda q, category_id: 1
        result = category_service.lock(category, get_data_func)
        expected = Category(id=1, name="PeshoStuff", is_private=0, is_locked=1)
        self.assertEqual(result, expected)

    def test_Unlock_returnsUnlockedCategory(self):
        category = Category(id=1, name="PeshoStuff", is_private=0, is_locked=1)
        get_data_func = lambda q, category_id: 1
        result = category_service.unlock(category, get_data_func)
        expected = Category(id=1, name="PeshoStuff", is_private=0, is_locked=0)
        self.assertEqual(result, expected)

    def test_MakePrivate_returnsPrivateCategory(self):
        category = Category(id=1, name="PeshoStuff", is_private=0, is_locked=0)
        get_data_func = lambda q, category_id: 1
        result = category_service.make_private(category, get_data_func)
        expected = Category(id=1, name="PeshoStuff", is_private=1, is_locked=0)
        self.assertEqual(result, expected)

    def test_MakePublic_returnsPublicCategory(self):
        category = Category(id=1, name="PeshoStuff", is_private=1, is_locked=0)
        get_data_func = lambda q, category_id: 1
        result = category_service.make_public(category, get_data_func)
        expected = Category(id=1, name="PeshoStuff", is_private=0, is_locked=0)
        self.assertEqual(result, expected)

    def test_GiveAccess_GivesAccessCorrectly(self):
        user = fake_user()
        permission = "w"
        category = Category(id=1, name="PeshoStuff", is_private=1, is_locked=0)
        read_data_func = lambda q, params: category
        update_data_func = (
            lambda q, params: f"User with id: {user.id} now has write permission to category {category.name}"
        )
        result = category_service.give_access(
            category, user.id, permission, read_data_func, update_data_func
        )
        expected = f"User with id: {user.id} now has write permission to category {category.name}"
        self.assertEqual(result, expected)

    def test_RevokeAccess_revokesAccessCorrectly(self):
        user = fake_user()
        category = Category(id=1, name="PeshoStuff", is_private=1, is_locked=0)
        read_data_func = lambda q, params: category
        update_data_func = (
            lambda q, params: f"User with id: {user.id} now has no permission to category {category.name}"
        )
        result = category_service.revoke_access(
            category, user.id, read_data_func, update_data_func
        )
        expected = (
            f"User with id: {user.id} now has no permission to category {category.name}"
        )
        self.assertEqual(result, expected)

    def test_RevokeAccess_returnsErrorWhenAccessNotFound(self):
        user = fake_user()
        category = Category(id=1, name="PeshoStuff", is_private=1, is_locked=0)
        read_data_func = lambda q, params: None
        update_data_func = (
            lambda q, params: f"This user has no rights for this category"
        )
        result = category_service.revoke_access(
            category, user.id, read_data_func, update_data_func
        )
        expected = f"This user has no rights for this category"
        self.assertEqual(result, expected)
