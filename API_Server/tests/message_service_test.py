import unittest
from unittest.mock import Mock
from datetime import datetime
from data.models import Message, CreatedMessage, User, Role
from services import message_service


def fake_recipient(
    id: int | None = 3,
    username="Peter",
    email="peter@abv.bg",
    password="Pepi123*",
    role=Role.ADMIN,
):
    mock_recipient = Mock(spec=User)
    mock_recipient.id = id
    mock_recipient.username = username
    mock_recipient.email = email
    mock_recipient.password = password
    mock_recipient.role = role
    return mock_recipient


def fake_message(
    id=1,
    content="fake content",
    sent_on=datetime(2022, 2, 2),
    author_id=1,
    recipient_id=3,
):
    mock_message = Mock(spec=Message)
    mock_message.id = id
    mock_message.content = content
    mock_message.sent_on = sent_on
    mock_message.author_id = author_id
    mock_message.recipient_id = recipient_id
    return mock_message


def fake_created_message(
    content="fake created message content", recipient_username="Gosho"
):
    mock_created_message = Mock(CreatedMessage)
    mock_created_message.content = content
    mock_created_message.recipient_username = recipient_username
    return mock_created_message


class MessageService_Should(unittest.TestCase):
    def test_create_message_returnsMessage(self):
        created_message = fake_created_message()
        from_db_message = fake_message()
        recipient = fake_recipient()
        author_id = 1

        find_u_func = lambda user_name: recipient
        insert_func = lambda q, p: 1
        read_func = lambda q, p: [
            (
                from_db_message.id,
                from_db_message.content,
                from_db_message.sent_on,
                from_db_message.author_id,
                from_db_message.recipient_id,
            )
        ]

        result = message_service.create(
            created_message,
            author_id=author_id,
            insert_data_func=insert_func,
            read_data_func=read_func,
            find_user_func=find_u_func,
        )

        expected = Message(
            id=from_db_message.id,
            content=from_db_message.content,
            sent_on=from_db_message.sent_on,
            author_id=from_db_message.author_id,
            recipient_id=from_db_message.recipient_id,
        )
        self.assertEqual(expected, result)
