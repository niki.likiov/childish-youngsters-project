from datetime import datetime
import unittest
from unittest.mock import Mock

from common.responses import BadRequest
from data.models import Category, RegisterData, User, LoginData
from routers import users as users_router

mock_user_service = Mock(spec="services.users_service")
mock_category_service = Mock(spec="services.category_service")
mock_auth = Mock(spec="common.auth")
users_router.users_service = mock_user_service
users_router.category_service = mock_category_service
users_router.get_user_or_raise_401 = mock_auth


def fake_user(
    id=1,
    name="Peter",
    surname="Petrov",
    username="Pesho",
    email="pesho@abv.bg",
    password="Pesho123%",
    role="member",
    created_on=datetime(2000, 2, 2, 2, 2, 2),
    mentor_id=None,
):
    mock_user = Mock(spec=User)
    mock_user.id = id
    mock_user.name = name
    mock_user.surname = surname
    mock_user.username = username
    mock_user.email = email
    mock_user.password = password
    mock_user.role = role
    mock_user.created_on = created_on
    mock_user.mentor_id = mentor_id

    return mock_user


class UserRouter_Should(unittest.TestCase):
    def setUp(self) -> None:
        mock_user_service.reset_mock()

    def test_Login_logsInSucessfully(self):
        user = fake_user()
        mock_user_service.try_login_with_username = lambda username, password: user

        data = LoginData(username=user.username, password=user.password)
        result = users_router.login(data)
        expected = f"User Pesho logged in"
        self.assertEqual(result, expected)

    def test_Login_returnsBadRequestOnInvalidData(self):
        user = fake_user()
        mock_user_service.try_login_with_username = lambda username, password: None

        data = LoginData(username=user.username, password=user.password)
        result = getattr(users_router.login(data), "body")
        expected = getattr(BadRequest("Invalid login data"), "body")
        self.assertEqual(result, expected)

    def test_Info_returnsUserInfoSuccessfully_withUsername(self):
        user = fake_user()
        mock_user_service.find_by_username = lambda username: user

        result = users_router.user_info(
            username=user.username,
        )
        expected = user
        self.assertEqual(result, expected)

    def test_Info_returnsUserInfoSuccessfully_withUserID(self):
        user = fake_user()
        mock_user_service.find_by_id = lambda id: user

        result = users_router.user_info(id=user.id, username=None)
        expected = user
        self.assertEqual(result, expected)

    def test_Info_returnsNone_onWrongData(self):
        user = fake_user()
        mock_user_service.find_by_id = lambda id: None

        result = users_router.user_info(id=user.id, username=None)
        expected = None
        self.assertEqual(result, expected)

    def test_Register_returnsDataSuccessfully(self):
        user = fake_user()
        x_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyIzIjoiUGV0ZXIifQ.2Q1jBTLK-NtIm_IoaFhAXQToLIKGZ9WjrVb-_YHloRY"
        mock_user_service.create = lambda name, surname, username, email, password: {
            user,
            x_token,
        }

        data = RegisterData(
            name=user.name,
            surname=user.surname,
            username=user.username,
            email=user.email,
            password=user.password,
        )
        result = users_router.register(data)
        expected = {user, x_token}
        self.assertEqual(result, expected)

    def test_Register_returnsBadRequest_onDuplicateUsernameEntry(self):
        user = fake_user()
        mock_user_service.create = (
            lambda name, surname, username, email, password: f"Error: Duplicate entry '{data.username}' for key 'username_UNIQUE'"
        )

        data = RegisterData(
            name=user.name,
            surname=user.surname,
            username=user.username,
            email=user.email,
            password=user.password,
        )
        result = getattr((users_router.register(data)), "body")
        expected = getattr(
            BadRequest(
                f"Username {data.username} is taken. Please choose a different username"
            ),
            "body",
        )
        self.assertEqual(result, expected)

    def test_Register_returnsBadRequest_onDuplicateEmailEntry(self):
        user = fake_user()
        mock_user_service.create = (
            lambda name, surname, username, email, password: f"Error: Duplicate entry '{data.email}' for key 'email_UNIQUE'"
        )

        data = RegisterData(
            name=user.name,
            surname=user.surname,
            username=user.username,
            email=user.email,
            password=user.password,
        )
        result = getattr((users_router.register(data)), "body")
        expected = getattr(
            BadRequest(
                f"Email {data.email} is taken. Please choose a different e-mail"
            ),
            "body",
        )
        self.assertEqual(result, expected)

    def test_logout_returnsDataSuccessfully(self):
        result = type(users_router.logout())
        expected = type(f"User username logged out")
        self.assertEqual(result, expected)

    def test_MakeAdmin_returnsDataSuccessfully(self):
        user = User(
            id=3,
            name="Peter",
            surname="Petrov",
            username="Pesho3",
            email="pesho3@abv.bg",
            password="Pesho123#",
            role="member",
            created_on=datetime(2000, 2, 2, 2, 2, 2),
            mentor_id=None,
        )
        mock_user_service.find_by_username = lambda username: user
        mock_user_service.adminize = lambda user: user
        result = users_router.make_admin(username=user.username)
        expected = {f"User {user.username} is now admin"}
        self.assertEqual(result, expected)

    def test_MakeAdmin_returnsBadRequest_ifUserAlreadyAdmin(self):
        user = User(
            id=3,
            name="Peter",
            surname="Petrov",
            username="Pesho3",
            email="pesho3@abv.bg",
            password="Pesho123#",
            role="admin",
            created_on=datetime(2000, 2, 2, 2, 2, 2),
            mentor_id=None,
        )
        mock_user_service.find_by_username = lambda username: user
        mock_user_service.adminize = lambda user: user
        result = getattr((users_router.make_admin(username=user.username)), "body")
        expected = getattr(BadRequest(f"User {user.username} is already admin"), "body")
        self.assertEqual(result, expected)

    def test_MakeAdmin_returnsBadRequest_ifThereIsNoSuchUserToBeAdmin(self):
        user = User(
            id=3,
            name="Peter",
            surname="Petrov",
            username="Pesho3",
            email="pesho3@abv.bg",
            password="Pesho123#",
            role="member",
            created_on=datetime(2000, 2, 2, 2, 2, 2),
            mentor_id=None,
        )
        mock_user_service.find_by_username = lambda username: None
        result = getattr((users_router.make_admin(username=user.username)), "body")
        expected = getattr(
            (BadRequest(f"There is no such user: {user.username}")), "body"
        )
        self.assertEqual(result, expected)

    def test_getPrivileged_returnsDataSuccessfully(self):
        user = fake_user()
        category = Category(id=5, name="Engines", is_private=1, is_locked=0)
        mock_category_service.get_by_id = lambda id: category
        mock_user_service.privileged_by_category = lambda category: user

        result = users_router.get_privileged(category.id)
        expected = user
        self.assertEqual(result, expected)

    def test_getPrivileged_returnsBadRequest_ifCategoryNotPrivate(self):
        category = Category(id=5, name="Engines", is_private=0, is_locked=0)
        mock_category_service.get_by_id = lambda id: category

        result = getattr((users_router.get_privileged(category.id)), "body")
        expected = getattr(
            BadRequest(
                f"Category with id: {category.id} - {category.name} is not private"
            ),
            "body",
        )
        self.assertEqual(result, expected)

    def test_getPrivileged_returnsBadRequest_ifNoCategory(self):
        category = Category(id=5, name="Engines", is_private=0, is_locked=0)
        mock_category_service.get_by_id = lambda id: None

        result = getattr(users_router.get_privileged(category.id), "body")
        expected = getattr(
            BadRequest(f"There is no category with id: {category.id}"), "body"
        )
        self.assertEqual(result, expected)
