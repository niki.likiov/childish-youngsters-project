import unittest
from datetime import datetime
from unittest.mock import Mock
from routers.topics import topics_router
from data.models import Topic, CreatedTopic
from services import topics_service

mock_topics_service = Mock(spec="services.topics_service")
topics_router.users_service = mock_topics_service


def fake_topic(
    id=1,
    title="Mock title",
    is_locked=0,
    created_on=datetime(2022, 1, 1, 0, 0),
    author_id=3,
    category_id=2,
):
    mock_topic = Mock(spec=Topic)
    mock_topic.id = id
    mock_topic.title = title
    mock_topic.is_locked = is_locked
    mock_topic.created_on = created_on
    mock_topic.author_id = author_id
    mock_topic.category_id = category_id
    return mock_topic


def fake_created_topic(
    title="Mock title here", category_id=1, first_reply="Mock first reply"
):
    mock_created_topic = Mock(spec=CreatedTopic)
    mock_created_topic.title = title
    mock_created_topic.category_id = category_id
    mock_created_topic.first_reply = first_reply
    return mock_created_topic


class TopicService_Should(unittest.TestCase):
    def setUp(self) -> None:
        mock_topics_service.reset_mock()

    def test_all_createsListOfTopics_when_dataIsPresent(self):
        get_data_func = lambda q: [
            (1, "Mock_Title", 1, datetime(2022, 1, 1, 0, 0), 3, 1),
            (2, "Mock_Title", 1, datetime(2022, 1, 1, 0, 0), 3, 2),
        ]
        result = list(
            topics_service.all(
                get_data_func=get_data_func,
                search=None,
                limit=None,
                offset=None,
                category_id=None,
            )
        )
        self.assertEqual(2, len(result))

    def test_findById_returnsTopicCorrectly(self):
        get_data_func = lambda i, id: [
            (1, "Mock_Title", 1, datetime(2022, 1, 1, 0, 0), 3, 1)
        ]

        result = topics_service.find_by_id(1, get_data_func)

        expected = Topic(
            id=1,
            title="Mock_Title",
            is_locked=1,
            created_on=datetime(2022, 1, 1, 0, 0),
            author_id=3,
            category_id=1,
        )
        self.assertEqual(expected, result)

    def test_lock_returnsTrue(self):
        topic = Topic(
            id=1,
            title="Mock_title",
            is_locked=0,
            created_on=datetime(2022, 1, 1, 0, 0),
            author_id=3,
            category_id=1,
        )
        update_data_func = lambda q, id: 1

        result = topics_service.lock(topic, update_data_func)
        expected = Topic(
            id=1,
            title="Mock_title",
            is_locked=1,
            created_on=datetime(2022, 1, 1, 0, 0),
            author_id=3,
            category_id=1,
        )
        self.assertEqual(expected, result)

    def test_unlock_returnsTrue(self):
        topic = Topic(
            id=1,
            title="Mock_title",
            is_locked=1,
            created_on=datetime(2022, 1, 1, 0, 0),
            author_id=3,
            category_id=1,
        )
        update_data_func = lambda q, id: 1

        result = topics_service.unlock(topic, update_data_func)
        expected = Topic(
            id=1,
            title="Mock_title",
            is_locked=0,
            created_on=datetime(2022, 1, 1, 0, 0),
            author_id=3,
            category_id=1,
        )
        self.assertEqual(expected, result)

    def test_createTopic_returnsTopicSucessfully(self):
        Mock_Created_Topic = fake_created_topic()
        read_data_func = lambda i, id: [
            (
                1,
                "Mock_Title",
                1,
                datetime(2022, 1, 1, 0, 0),
                3,
                1,
            )
        ]
        insert_data_func = lambda q, params: 1235
        author_id = 3
        result = topics_service.create(
            Mock_Created_Topic, author_id, read_data_func, insert_data_func
        )
        expected = Topic(
            id=1,
            title="Mock_Title",
            is_locked=1,
            created_on=datetime(2022, 1, 1, 0, 0),
            author_id=3,
            category_id=1,
        )
        self.assertEqual(expected, result)
