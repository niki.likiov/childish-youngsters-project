import unittest
from unittest.mock import Mock
from datetime import datetime

from common.responses import BadRequest, NotFound
from data.models import Category, Topic
from routers import categories as categories_router
from tests.user_router_test import fake_user


mock_category_service = Mock(spec="services.category_service")
mock_topic_service = Mock(spec="services.topic_service")
mock_user_service = Mock(spec="services.user_service")
mock_auth = Mock(spec="common.auth")
categories_router.category_service = mock_category_service
categories_router.topics_service = mock_topic_service
categories_router.users_service = mock_user_service
categories_router.get_user_or_raise_401 = mock_auth


def fake_category(id=1, name="Peshovshit", is_private=0, is_locked=0):
    mock_category = Mock(spec=Category)
    mock_category.id = id
    mock_category.name = name
    mock_category.is_private = is_private
    mock_category.is_locked = is_locked
    return mock_category


class CategoryRouter_Should(unittest.TestCase):
    def setUp(self) -> None:
        mock_category_service.reset_mock()

    def test_GetCategories_returnsDataforGuestIfNoToken(self):
        mock_category_service.all_for_guest = lambda: (1, "Peshoshit", 0, 0)

        result = list(categories_router.get_categories(x_token=None))
        expected = [1, "Peshoshit", 0, 0]
        self.assertEqual(result, expected)

    def test_GetCategories_returnsDataforAdminIfToken(self):
        mock_category_service.all_for_admin = lambda: (1, "Peshoshit", 0, 0)

        result = categories_router.get_categories()
        expected = (1, "Peshoshit", 0, 0)
        self.assertEqual(result, expected)

        ###################def test_GetCategories_returnsDataforUserIfToken(self): ###################################
        mock_category_service.all_for_user = lambda: (1, "Peshoshit", 0, 0)

        result = list(categories_router.get_categories())
        expected = [1, "Peshoshit", 0, 0]
        self.assertEqual(result, expected)

    def test_GetCategoryById_returnsListOfTopics_onCorrectData(self):
        topic = Topic(
            id=1,
            title="Пране",
            is_locked=True,
            created_on=datetime(2022, 2, 2, 2, 2, 2),
            author_id=2,
            category_id=3,
        )
        category = fake_category()
        mock_category_service.get_by_id = lambda id: category
        mock_topic_service.all = lambda search, sort, sort_by, id: topic

        result = categories_router.get_category_by_id(category.id)
        expected = topic
        self.assertEqual(result, expected)

    def test_GetCategoryById_returnsNotFound_onInCorrectData(self):
        topic = Topic(
            id=1,
            title="Пране",
            is_locked=True,
            created_on=datetime(2022, 2, 2, 2, 2, 2),
            author_id=2,
            category_id=3,
        )
        category = fake_category()
        mock_category_service.get_by_id = lambda id: None
        mock_topic_service.all = lambda search, sort, sort_by, id: topic

        result = type(categories_router.get_category_by_id(category.id))
        expected = type(NotFound())
        self.assertEqual(result, expected)

    def test_Create_returnsCategory_WhenCorrectData(self):
        category = fake_category()
        mock_category_service.create = lambda id: category

        result = categories_router.create_category(category)
        expected = category
        self.assertEqual(result, expected)

    def test_Create_returnsBadRequest_WhenDuplicateEntry(self):
        category = fake_category()
        mock_category_service.create = (
            lambda id: f"mariadb.IntegrityError: Duplicate entry '{category.name}' for key 'name_UNIQUE'"
        )

        result = getattr(categories_router.create_category(category), "body")
        expected = getattr(
            BadRequest(f"Category name '{category.name}' already exists"), "body"
        )
        self.assertEqual(result, expected)

    def test_Lock_returnsCategory_WhenCorrectData(self):
        category = Category(id=1, name="Пране", is_private=0, is_locked=0)
        mock_category_service.exists = lambda id: True
        mock_category_service.get_by_id = lambda id: category
        mock_category_service.lock = lambda category: Category(
            id=1, name="Пране", is_private=0, is_locked=1
        )

        result = categories_router.lock_category(category.id)
        expected = Category(id=1, name="Пране", is_private=0, is_locked=1)
        self.assertEqual(result, expected)

    def test_Lock_returnsBadRequest_WhenCategoryNonExistent(self):
        category = Category(id=1, name="Пране", is_private=0, is_locked=0)
        mock_category_service.exists = lambda id: False
        mock_category_service.get_by_id = lambda id: category
        mock_category_service.lock = lambda category: Category(
            id=1, name="Пране", is_private=0, is_locked=1
        )

        result = getattr(categories_router.lock_category(category.id), "body")
        expected = getattr(
            BadRequest(f"Category with id {category.id} does not exist"), "body"
        )
        self.assertEqual(result, expected)

    def test_Unlock_returnsCategory_WhenCorrectData(self):
        category = Category(id=1, name="Пране", is_private=0, is_locked=1)
        mock_category_service.exists = lambda id: True
        mock_category_service.get_by_id = lambda id: category
        mock_category_service.unlock = lambda category: Category(
            id=1, name="Пране", is_private=0, is_locked=0
        )

        result = categories_router.unlock_category(category.id)
        expected = Category(id=1, name="Пране", is_private=0, is_locked=0)
        self.assertEqual(result, expected)

    def test_Unlock_returnsBadRequest_WhenCategoryNonExistent(self):
        category = Category(id=1, name="Пране", is_private=0, is_locked=1)
        mock_category_service.exists = lambda id: False
        mock_category_service.get_by_id = lambda id: category
        mock_category_service.unlock = lambda category: Category(
            id=1, name="Пране", is_private=0, is_locked=0
        )

        result = getattr(categories_router.unlock_category(category.id), "body")
        expected = getattr(
            BadRequest(f"Category with id {category.id} does not exist"), "body"
        )
        self.assertEqual(result, expected)

    def test_MakePrivate_returnsCategory_WhenCorrectData(self):
        category = Category(id=1, name="Пране", is_private=0, is_locked=0)
        mock_category_service.exists = lambda id: True
        mock_category_service.get_by_id = lambda id: category
        mock_category_service.make_private = lambda category: Category(
            id=1, name="Пране", is_private=1, is_locked=0
        )

        result = categories_router.make_category_private(category.id)
        expected = Category(id=1, name="Пране", is_private=1, is_locked=0)
        self.assertEqual(result, expected)

    def test_MakePrivate_returnsBadRequest_WhenCategoryNonExistent(self):
        category = Category(id=1, name="Пране", is_private=1, is_locked=0)
        mock_category_service.exists = lambda id: False
        mock_category_service.get_by_id = lambda id: category
        mock_category_service.make_private = lambda category: Category(
            id=1, name="Пране", is_private=1, is_locked=0
        )

        result = getattr(categories_router.unlock_category(category.id), "body")
        expected = getattr(
            BadRequest(f"Category with id {category.id} does not exist"), "body"
        )
        self.assertEqual(result, expected)

    def test_MakePublic_returnsCategory_WhenCorrectData(self):
        category = Category(id=1, name="Пране", is_private=1, is_locked=0)
        mock_category_service.exists = lambda id: True
        mock_category_service.get_by_id = lambda id: category
        mock_category_service.make_public = lambda category: Category(
            id=1, name="Пране", is_private=0, is_locked=0
        )

        result = categories_router.make_category_public(category.id)
        expected = Category(id=1, name="Пране", is_private=0, is_locked=0)
        self.assertEqual(result, expected)

    def test_MakePublic_returnsBadRequest_WhenCategoryNonExistent(self):
        category = Category(id=1, name="Пране", is_private=1, is_locked=0)
        mock_category_service.exists = lambda id: False
        mock_category_service.get_by_id = lambda id: category
        mock_category_service.make_private = lambda category: Category(
            id=1, name="Пране", is_private=0, is_locked=0
        )

        result = getattr(categories_router.unlock_category(category.id), "body")
        expected = getattr(
            BadRequest(f"Category with id {category.id} does not exist"), "body"
        )
        self.assertEqual(result, expected)

    def test_GiveAccess_returnsCategory_WhenCorrectData(self):
        category = fake_category()
        user = fake_user()
        mock_category_service.exists = lambda id: True
        mock_category_service.get_by_id = lambda id: category
        mock_user_service.find_by_username = lambda username: user
        mock_category_service.give_access = lambda cat, username, permission: (
            f"User with id: {user.id} now has {permission} permission to category {category.name}"
        )

        result = categories_router.give_access_to_user(
            category.id, user.username, permission="w"
        )
        expected = (
            f"User with id: {user.id} now has w permission to category {category.name}"
        )
        self.assertEqual(result, expected)

    def test_GiveAccess_returnsBadRequest_WhenUserNonExistent(self):
        category = fake_category()
        user = fake_user()
        mock_category_service.exists = lambda id: True
        mock_category_service.get_by_id = lambda id: category
        mock_user_service.find_by_username = lambda username: None
        mock_category_service.give_access = lambda cat, username, permission: (
            f"User with id: {user.id} now has {permission} permission to category {category.name}"
        )

        result = getattr(
            categories_router.give_access_to_user(
                category.id, user.username, permission="w"
            ),
            "body",
        )
        expected = getattr(BadRequest(f"No user with username {user.username}"), "body")
        self.assertEqual(result, expected)

    def test_GiveAccess_returnsBadRequest_WhenCategoryNonExistent(self):
        category = fake_category()
        user = fake_user()
        mock_category_service.exists = lambda id: False
        mock_category_service.get_by_id = lambda id: category
        mock_user_service.find_by_username = lambda username: user
        mock_category_service.give_access = lambda cat, username, permission: (
            f"User with id: {user.id} now has {permission} permission to category {category.name}"
        )

        result = getattr(
            categories_router.give_access_to_user(
                category.id, user.username, permission="w"
            ),
            "body",
        )
        expected = getattr(
            BadRequest(f"Category with id {category.id} does not exist"), "body"
        )
        self.assertEqual(result, expected)

    def test_RevokeAccess_returnsCategory_WhenCorrectData(self):
        category = fake_category()
        user = fake_user()
        mock_category_service.exists = lambda id: True
        mock_category_service.get_by_id = lambda id: category
        mock_user_service.find_by_username = lambda username: user
        mock_category_service.revoke_access = lambda cat, username: (
            f"User with id: {user.id} now has no permission to category {category.name}"
        )

        result = categories_router.revoke_access_from_user(category.id, user.username)
        expected = (
            f"User {user.username} now has no permission to category {category.name}"
        )
        self.assertEqual(result, expected)

    def test_RevokeAccess_returnsBadRequest_WhenUserNonExistent(self):
        category = fake_category()
        user = fake_user()
        mock_category_service.exists = lambda id: True
        mock_category_service.get_by_id = lambda id: category
        mock_user_service.find_by_username = lambda username: None
        mock_category_service.revoke_access = lambda cat, username: (
            f"User with id: {user.id} now has no permission to category {category.name}"
        )

        result = getattr(
            categories_router.revoke_access_from_user(category.id, user.username),
            "body",
        )
        expected = getattr(BadRequest(f"No user with username {user.username}"), "body")
        self.assertEqual(result, expected)

    def test_revokeAccess_returnsBadRequest_WhenCategoryNonExistent(self):
        category = fake_category()
        user = fake_user()
        mock_category_service.exists = lambda id: False
        mock_category_service.get_by_id = lambda id: category
        mock_user_service.find_by_username = lambda username: user
        mock_category_service.revoke_access = lambda cat, username: (
            f"User with id: {user.id} now has no permission to category {category.name}"
        )

        result = getattr(
            categories_router.revoke_access_from_user(category.id, user.username),
            "body",
        )
        expected = getattr(
            BadRequest(f"Category with id {category.id} does not exist"), "body"
        )
        self.assertEqual(result, expected)

    def test_revokeAccess_returnsBadRequest_WhenUserHasNoRightsForCategory(self):
        category = fake_category()
        user = fake_user()
        mock_category_service.exists = lambda id: True
        mock_category_service.get_by_id = lambda id: category
        mock_user_service.find_by_username = lambda username: user
        mock_category_service.revoke_access = lambda cat, username: (f"Errrrrrrror")

        result = getattr(
            categories_router.revoke_access_from_user(category.id, user.username),
            "body",
        )
        expected = getattr(
            BadRequest(
                f"User {user.username} has no rights for category {category.name}"
            ),
            "body",
        )
        self.assertEqual(result, expected)
