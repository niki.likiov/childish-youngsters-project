from datetime import datetime
from mariadb import IntegrityError
import unittest
from unittest.mock import Mock
from data.models import Category, User
from services import users_service
from routers import users as users_router
import jwt

key = "Super"

mock_user_service = Mock(spec="services.users_service")
users_router.users_service = mock_user_service


def fake_user(
    id=1,
    name="Petar",
    surname="Petrov",
    username="Pesho",
    email="pesho@abv.bg",
    password="Pesho",
    role="member",
    created_on=datetime(2000, 2, 2, 2, 2, 2),
    mentor_id=None,
):
    mock_user = Mock(spec=User)
    mock_user.id = id
    mock_user.name = name
    mock_user.surname = surname
    mock_user.username = username
    mock_user.email = email
    mock_user.password = password
    mock_user.role = role
    mock_user.created_on = created_on
    mock_user.mentor_id = mentor_id

    return mock_user


def fake_category(id=1, name="PeshovStuff", is_private=0, is_locked=0):
    mock_category = Mock(spec=Category)
    mock_category.id = id
    mock_category.name = name
    mock_category.is_private = is_private
    mock_category.is_locked = is_locked
    return mock_category


def _hash_password(password: str):
    from hashlib import sha256

    return sha256(password.encode("utf-8")).hexdigest()


class UserService_Should(unittest.TestCase):
    def setUp(self) -> None:
        mock_user_service.reset_mock()

    def test_findByUsername_returnsUserCorrectly(self):
        get_userdata_func = lambda u, username: [
            (
                1,
                "Petar",
                "Petrov",
                "Pesho",
                "pesho@abv.bg",
                "Pesho",
                "member",
                datetime(2000, 2, 2, 2, 2, 2),
                None,
            )
        ]
        result = users_service.find_by_username("Pesho", get_userdata_func)

        expected = User(
            id=1,
            name="Petar",
            surname="Petrov",
            username="Pesho",
            email="pesho@abv.bg",
            password="Pesho",
            role="member",
            created_on=datetime(2000, 2, 2, 2, 2, 2),
            mentor_id=None,
        )
        self.assertEqual(expected, result)

    def test_findByUsername_returnsNoData_ifUsernameNotFound(self):
        get_userdata_func = lambda u, username: []
        result = users_service.find_by_username("Pesho", get_userdata_func)
        expected = None
        self.assertEqual(expected, result)

    def test_findById_returnsUserCorrectly(self):
        get_userdata_func = lambda u, username: [
            (
                1,
                "Petar",
                "Petrov",
                "Pesho",
                "pesho@abv.bg",
                "Pesho",
                "member",
                datetime(2000, 2, 2, 2, 2, 2),
                None,
            )
        ]
        result = users_service.find_by_id("Pesho", get_userdata_func)

        expected = User(
            id=1,
            name="Petar",
            surname="Petrov",
            username="Pesho",
            email="pesho@abv.bg",
            password="Pesho",
            role="member",
            created_on=datetime(2000, 2, 2, 2, 2, 2),
            mentor_id=None,
        )
        self.assertEqual(expected, result)

    def test_findById_returnsNoData_ifUserIdNotFound(self):
        get_userdata_func = lambda q, id: []
        result = users_service.find_by_username("Pesho", get_userdata_func)
        expected = None
        self.assertEqual(expected, result)

    def test_tryLoginWithEmail_returnsUserCorrectly(self):
        Mock_Pesho = fake_user()
        get_userdata_func = lambda username: Mock_Pesho

        password = _hash_password("Pesho")
        Mock_Pesho.password = password
        result = users_service.try_login_with_username(
            "Pesho", "Pesho", get_userdata_func
        )

        expected = Mock_Pesho
        self.assertEqual(expected, result)

    def test_tryLoginWithUsername_returnsNone_on_wrong_password(self):
        get_userdata_func = lambda username: None
        result = users_service.try_login_with_username(
            "blqblq", "asdfasdf", get_userdata_func
        )
        expected = None
        self.assertEqual(expected, result)

    def test_createUser_returnsUserSucessfully(self):
        Mock_Pesho = fake_user()
        get_data_func = lambda q, params: Mock_Pesho.id

        result = users_service.create(
            "Petar", "Petrov", "Pesho", "pesho@abv.bg", "Pesho", get_data_func
        )
        hashed_password = _hash_password("Pesho")
        token = jwt.encode({Mock_Pesho.id: Mock_Pesho.username}, key, algorithm="HS256")
        expected = {
            "User": User(
                id=Mock_Pesho.id,
                name=Mock_Pesho.name,
                surname=Mock_Pesho.surname,
                username=Mock_Pesho.username,
                email=Mock_Pesho.email,
                password=hashed_password,
                role=Mock_Pesho.role,
                created_on=None,
                mentor_id=None,
            ),
            "token": token,
        }
        self.assertEqual(expected, result)

    def test_createUser_returnsErrorOnDuplicateEntry(self):
        def get_userdata_func(q, params):
            raise IntegrityError

        result = users_service.create(
            "Petar", "Petrov", "Pesho", "pesho@abv.bg", "Pesho", get_userdata_func
        )

        expected = "Error: "
        self.assertEqual(expected, result)

    def test_adminize_returnsUserAdmin_Correctly(self):
        Mock_Pesho = fake_user()
        get_data_func = lambda q, params: Mock_Pesho

        result = users_service.adminize(Mock_Pesho, get_data_func)
        Mock_Pesho.role = "admin"
        expected = Mock_Pesho
        self.assertEqual(expected, result)
        self.assertEqual(Mock_Pesho.role, "admin")

    def test_isAuthenticated_returnsTrueOnCorrectToken(self):
        Mock_Pesho = fake_user()
        token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyIzIjoiUGV0ZXIifQ.2Q1jBTLK-NtIm_IoaFhAXQToLIKGZ9WjrVb-_YHloRY"
        Mock_Pesho.id = 3
        get_data_func = lambda username: Mock_Pesho

        result = users_service.is_authenticated(token, get_data_func)

        expected = True
        self.assertEqual(expected, result)

    def test_isAuthenticated_returnsFalseOnWrongToken(self):
        Mock_Pesho = fake_user()
        token = "asdasdasd"
        get_data_func = lambda username: Mock_Pesho

        result = users_service.is_authenticated(token, get_data_func)

        expected = False
        self.assertEqual(expected, result)

    def test_viewPrivilegedUsers_returnsUsersCorrectly(self):
        Mock_Pesho = fake_user()
        category = fake_category()
        get_data_func = lambda q, params: [
            (1, "Petar", "Petrov", "Pesho", "pesho@abv.bg", "", "admin", None, None),
            (
                11,
                "Petar",
                "Petrov",
                "Pesho11",
                "pesho11@abv.bg",
                "",
                "member",
                None,
                None,
            ),
        ]
        result = list(users_service.privileged_by_category(category, get_data_func))
        expected = [
            User(
                id=1,
                name="Petar",
                surname="Petrov",
                username="Pesho",
                email="pesho@abv.bg",
                password="",
                role="admin",
                created_on=None,
                mentor_id=None,
            ),
            User(
                id=11,
                name="Petar",
                surname="Petrov",
                username="Pesho11",
                email="pesho11@abv.bg",
                password="",
                role="member",
                created_on=None,
                mentor_id=None,
            ),
        ]
        self.assertEqual(expected, result)
