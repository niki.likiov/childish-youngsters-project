from requests import get
from data.database import insert_query, read_query, update_query
from data.models import Topic, CreatedTopic


def all(
    search: str | None = None,
    limit: int | None = None,
    offset: int | None = None,
    category_id: int | None = None,
    get_data_func=None,
):
    if get_data_func == None:
        get_data_func = read_query
    if category_id == None:
        if search == None:
            if offset == None:
                if limit == None:
                    data = get_data_func(
                        """SELECT id, title, is_locked, created_on, author_id, category_id FROM topics 
                        ORDER BY id """
                    )
                else:
                    data = get_data_func(
                        """SELECT id, title, is_locked, created_on, author_id, category_id FROM topics 
                        ORDER BY id limit ?""",
                        (limit,),
                    )
            else:
                data = get_data_func(
                    """SELECT id, title, is_locked, created_on, author_id, category_id FROM topics 
                    ORDER BY id limit ? offset ?""",
                    (
                        limit,
                        offset,
                    ),
                )
        else:
            if offset == None:
                if limit == None:
                    data = get_data_func(
                        """SELECT id, title, is_locked, created_on, author_id, category_id FROM topics 
                        WHERE title LIKE ? ORDER BY id""",
                        (f"%{search}%",),
                    )
                else:
                    data = get_data_func(
                        """SELECT id, title, is_locked, created_on, author_id, category_id FROM topics 
                        WHERE title LIKE ? ORDER BY id limit ?""",
                        (
                            f"%{search}%",
                            limit,
                        ),
                    )
            else:
                data = get_data_func(
                    """SELECT id, title, is_locked, created_on, author_id, category_id FROM topics 
                    WHERE title LIKE ? ORDER BY id limit ? offset ?""",
                    (
                        f"%{search}%",
                        limit,
                        offset,
                    ),
                )
    else:
        if search == None:
            if offset == None:
                if limit == None:
                    data = get_data_func(
                        """SELECT id, title, is_locked, created_on, author_id, category_id FROM 
                            topics where category_id=? ORDER BY id""",
                        (category_id,),
                    )
                else:
                    data = get_data_func(
                        """SELECT id, title, is_locked, created_on, author_id, category_id FROM 
                            topics where category_id=? ORDER BY id limit ?""",
                        (
                            category_id,
                            limit,
                        ),
                    )
            else:
                data = get_data_func(
                    """SELECT id, title, is_locked, created_on, author_id, category_id FROM topics 
                        where category_id=? ORDER BY id limit ? offset ?""",
                    (
                        category_id,
                        limit,
                        offset,
                    ),
                )
        else:
            if offset == None:
                if limit == None:
                    data = get_data_func(
                        """SELECT id, title, is_locked, created_on, author_id, category_id FROM 
                            topics WHERE title LIKE ? and category_id=? ORDER BY id""",
                        (
                            f"%{search}%",
                            category_id,
                        ),
                    )
                else:
                    data = get_data_func(
                        """SELECT id, title, is_locked, created_on, author_id, category_id FROM 
                            topics WHERE title LIKE ? and category_id=? ORDER BY id limit ?""",
                        (
                            f"%{search}%",
                            category_id,
                            limit,
                        ),
                    )
            else:
                data = get_data_func(
                    """SELECT id, title, is_locked, created_on, author_id, category_id FROM topics 
                        WHERE title LIKE ? and category_id=? ORDER BY id limit ? offset ?""",
                    (
                        f"%{search}%",
                        category_id,
                        limit,
                        offset,
                    ),
                )
    return (
        Topic(
            id=id,
            title=title,
            is_locked=is_locked,
            created_on=created_on,
            author_id=author_id,
            category_id=category_id,
        )
        for id, title, is_locked, created_on, author_id, category_id in data
    )


def sort(lst: list[Topic], *, attribute="title", reverse=False):
    if attribute == "title":

        def sort_fn(t: Topic):
            return t.title

    elif attribute == "created_on":

        def sort_fn(t: Topic):
            return t.created_on

    elif attribute == "author_id":

        def sort_fn(t: Topic):
            return t.author_id

    elif attribute == "category_id":

        def sort_fn(t: Topic):
            return t.category_id

    else:

        def sort_fn(t: Topic):
            return t.id

    return sorted(lst, key=sort_fn, reverse=reverse)


def exists(id: int, get_data_func=None) -> bool:
    if get_data_func == None:
        get_data_func = read_query
    return any(
        get_data_func(
            """SELECT id, title, is_locked, created_on, author_id, category_id FROM topics 
                          where id = ?""",
            (id,),
        )
    )


def find_by_id(id: int, get_data_func=None) -> Topic | None:
    if get_data_func == None:
        get_data_func = read_query
    data = get_data_func(
        """SELECT id, title, is_locked, created_on, author_id, category_id
        FROM topics WHERE id = ?""",
        (id,),
    )

    return next(
        (
            Topic(
                id=id,
                title=title,
                is_locked=is_locked,
                created_on=created_on,
                author_id=author_id,
                category_id=category_id,
            )
            for id, title, is_locked, created_on, author_id, category_id in data
        ),
        None,
    )


def create(topic: CreatedTopic, author_id, read_data_func=None, insert_data_func=None):
    if insert_data_func == None:
        insert_data_func = insert_query
    generated_topic_id = insert_data_func(
        """ INSERT INTO topics (title, author_id, category_id) 
    VALUES (?,?,?)""",
        (topic.title, author_id, topic.category_id),
    )
    if read_data_func == None:
        read_data_func = read_query
    new_topic = read_data_func(
        """SELECT * FROM topics WHERE id = ?""", (generated_topic_id,)
    )

    insert_data_func(
        """ INSERT INTO replies (text, author_id, topic_id) VALUES (?,?,?)""",
        (topic.first_reply, author_id, generated_topic_id),
    )

    return Topic.from_query_result(*new_topic[0])


def edit(old: Topic, new: Topic, update_data_func=None):
    merged = Topic(
        id=old.id,
        title=new.title or old.title,
        is_locked=new.is_locked or old.is_locked,
        created_on=new.created_on or old.created_on,
        author_id=new.author_id or old.author_id,
        category_id=new.category_id or old.category_id,
    )
    if update_data_func == None:
        update_data_func = update_query
    update_data_func(
        """UPDATE topics SET
           title = ?, is_locked = ?, created_on = ?, author_id = ?, category_id = ?
           WHERE id = ? 
        """,
        (
            merged.title,
            merged.is_locked,
            merged.created_on,
            merged.author_id,
            merged.category_id,
            merged.id,
        ),
    )

    return merged


def delete(topic_id, update_data_func=None):
    if update_data_func == None:
        update_data_func = update_query
    update_data_func("DELETE FROM topics WHERE id = ?", (topic_id,))


def lock(topic: Topic, update_data_func=None) -> Topic:
    if update_data_func == None:
        update_data_func = update_query

    updated_topic = update_data_func(
        """UPDATE topics SET topics.is_locked=1 where topics.id = ?""",
        (topic.id,),
    )
    if updated_topic > 0:
        topic.is_locked = 1
    return topic


def unlock(topic: Topic, update_data_func=None) -> Topic:
    if update_data_func == None:
        update_data_func = update_query
    updated_topic = update_data_func(
        """UPDATE topics SET topics.is_locked=0 where topics.id = ?""",
        (topic.id,),
    )
    if updated_topic > 0:
        topic.is_locked = 0
    return topic
