from mariadb import IntegrityError
from data.database import insert_query, read_query, update_query
from data.models import Category, Topic


def all_for_guest(get_data_func=None) -> list[Category]:
    if get_data_func == None:
        get_data_func = read_query
    data = get_data_func(
        "SELECT id, name, is_private, is_locked FROM categories WHERE is_private=0 order by id"
    )

    return (Category.from_query_result(*row) for row in data)


def all_for_user(user_id: int, get_data_func=None) -> list[Category]:
    if get_data_func == None:
        get_data_func = read_query
    data = get_data_func(
        """SELECT c.id, c.name, c.is_private, c.is_locked
        FROM categories AS c 
        JOIN users_categories as uc
        ON c.id=uc.category_id 
        WHERE uc.user_id=?
        UNION ALL
        SELECT id, name, is_private, is_locked 
        FROM categories 
        WHERE is_private = 0 order by id""",
        (user_id,),
    )

    return (Category.from_query_result(*row) for row in data)


def all_for_admin(get_data_func=None) -> list[Category]:
    if get_data_func == None:
        get_data_func = read_query
    data = get_data_func(
        "SELECT id, name, is_private, is_locked FROM categories order by id"
    )

    return (Category.from_query_result(*row) for row in data)


def get_by_id(id: int, get_data_func=None) -> Category | None:
    if get_data_func == None:
        get_data_func = read_query
    data = get_data_func(
        "select id, name, is_private, is_locked from categories where id = ?", (id,)
    )

    return next(
        (
            Category(id=id, name=name, is_private=is_private, is_locked=is_locked)
            for id, name, is_private, is_locked in data
        ),
        None,
    )


def get_topics(category_id: int, get_data_func=None) -> list[Topic]:
    if get_data_func == None:
        get_data_func = read_query
    data = get_data_func(
        "select id, title, is_locked, created_on, author_id, category_id, best_reply_id from topics where category_id=?",
        (category_id,),
    )
    return (Topic.from_query_result(*row) for row in data)


def exists(id: int, get_data_func=None) -> bool:
    if get_data_func == None:
        get_data_func = read_query
    return any(get_data_func("select id, name from categories where id = ?", (id,)))


def create(category_name: str, get_data_func=None) -> Category | IntegrityError:
    try:
        if get_data_func == None:
            get_data_func = insert_query
        generated_id = get_data_func(
            "insert into categories(name) values(?)",
            (category_name,),
        )
        return Category(
            id=generated_id,
            name=category_name,
            is_locked=0,
            is_private=0,
        )
    except IntegrityError as err:
        error = "Error: {}".format(err)
        return error


def lock(category: Category, get_data_func=None) -> Category:
    if get_data_func == None:
        get_data_func = update_query
    updated_category = get_data_func(
        "UPDATE categories SET categories.is_locked=1 where categories.id = ?",
        (category.id,),
    )
    if updated_category > 0:
        category.is_locked = 1
    return category


def unlock(category: Category, get_data_func=None) -> Category:
    if get_data_func == None:
        get_data_func = update_query
    updated_category = get_data_func(
        "UPDATE categories SET categories.is_locked=0 where categories.id = ?",
        (category.id,),
    )
    if updated_category > 0:
        category.is_locked = 0
    return category


def make_private(category: Category, get_data_func=None) -> Category:
    if get_data_func == None:
        get_data_func = update_query
    updated_category = get_data_func(
        "UPDATE categories SET categories.is_private=1 where categories.id = ?",
        (category.id,),
    )
    if updated_category > 0:
        category.is_private = 1
    return category


def make_public(category: Category, get_data_func=None) -> Category:
    if get_data_func == None:
        get_data_func = update_query
    updated_category = get_data_func(
        "UPDATE categories SET categories.is_private=0 where categories.id = ?",
        (category.id,),
    )
    if updated_category > 0:
        category.is_private = 0
    return category


def give_access(
    category: Category,
    user_id: int,
    permission: str,
    read_data_func=None,
    update_data_func=None,
):
    if read_data_func == None:
        read_data_func = read_query
    record = read_data_func(
        "SELECT user_id, category_id, permission FROM users_categories WHERE user_id=? and category_id=?",
        (
            user_id,
            category.id,
        ),
    )
    if update_data_func == None:
        update_data_func = update_query
    if record:
        update_data_func(
            "UPDATE users_categories SET users_categories.permission=? where user_id=? and category_id=?",
            (
                permission,
                user_id,
                category.id,
            ),
        )
        if permission == "r":
            rights = "read"
        else:
            rights = "write"
        return f"User with id: {user_id} now has {rights} permission to category {category.name}"
    else:
        update_data_func(
            "INSERT into users_categories(user_id, category_id, permission) VALUES (?,?,?)",
            (
                user_id,
                category.id,
                permission,
            ),
        )
        if permission == "r":
            rights = "read"
        else:
            rights = "write"
        return f"User with id: {user_id} now has {rights} permission to category {category.name}"


def revoke_access(
    category: Category, user_id: int, read_data_func=None, update_data_func=None
):
    if read_data_func == None:
        read_data_func = read_query
    record = read_data_func(
        "SELECT user_id, category_id, permission FROM users_categories WHERE user_id=? and category_id=?",
        (
            user_id,
            category.id,
        ),
    )
    if update_data_func == None:
        update_data_func = update_query
    if record:
        update_data_func(
            "DELETE from users_categories WHERE user_id=? and category_id=?",
            (
                user_id,
                category.id,
            ),
        )
        return (
            f"User with id: {user_id} now has no permission to category {category.name}"
        )
    else:
        return f"This user has no rights for this category"
