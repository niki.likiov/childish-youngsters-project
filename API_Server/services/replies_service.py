from data.database import delete_query, read_query, insert_query, update_query
from data.models import Reply, CreatedReply


def all() -> list[Reply]:
    data = read_query(
        """SELECT id, text, author_id, created_on, topic_id FROM replies ORDER BY id"""
    )

    return (
        Reply(
            id=id,
            text=text,
            author_id=author_id,
            created_on=created_on,
            topic_id=topic_id,
        )
        for id, text, author_id, created_on, topic_id in data
    )


def all_by_topic(topic_id: int) -> list[Reply]:
    data = read_query(
        """SELECT id, text, author_id, created_on, topic_id FROM replies WHERE topic_id=? ORDER BY id""",
        (topic_id,),
    )
    return (Reply.from_query_result(*row) for row in data)


def create(reply: CreatedReply, author_id, topic_id):
    generated_id = insert_query(
        """ INSERT INTO replies (text, author_id, topic_id) 
    VALUES (?,?,?)""",
        (reply.text, author_id, topic_id),
    )

    new_reply = read_query("""SELECT * FROM replies WHERE id = ?""", (generated_id,))

    return Reply.from_query_result(*new_reply[0])


def exists(id: int):
    return any(read_query("""SELECT id, title FROM replies WHERE id = ?""", (id,)))


def find_by_id(id: int):
    data = read_query(
        f"""SELECT id, text, author_id, created_on, topic_id
        FROM replies WHERE id = ?""",
        (id,),
    )

    return next(
        (
            Reply(
                id=id,
                text=text,
                author_id=author_id,
                created_on=created_on,
                topic_id=topic_id,
            )
            for id, text, author_id, created_on, topic_id in data
        ),
        None,
    )


def best_vote(user_id: int, reply: Reply):
    user_vote = read_query(
        """SELECT user_id, reply_id, vote_id FROM user_votes as uv JOIN replies AS r ON r.id=uv.reply_id JOIN topics AS t ON r.topic_id=t.id WHERE user_id=? and vote_id=3 and topic_id=?""",
        (user_id, reply.topic_id),
    )  # checks whether the user has given another best reply vote for a reply in the same topic
    if user_vote:
        reply_to_delete = user_vote[0][1]
        delete_query(
            """DELETE FROM user_votes WHERE user_id=? and reply_id=? and vote_id=3""",
            (user_id, reply_to_delete),
        )  # removes the previous best vote entry
    new_vote = insert_query(
        """INSERT into user_votes (user_id, reply_id, vote_id) 
    VALUES (?,?,?)""",
        (user_id, reply.id, 3),
    )
    return new_vote


def upvote(user_id: int, reply: Reply):
    user_vote = read_query(
        """SELECT user_id, reply_id, vote_id FROM user_votes WHERE user_id=? and reply_id=?""",
        (user_id, reply.id),
    )  # checks whether the user has given a downvote for the same reply before or upvote already exists
    if user_vote and user_vote[0][2] == 1:
        new_vote = update_query(
            """UPDATE user_votes SET vote_id=? WHERE user_id=? and reply_id=? and vote_id=1""",
            (2, user_id, reply.id),
        )
    elif user_vote and user_vote[0][2] == 2:
        return user_vote
    else:
        new_vote = insert_query(
            """INSERT into user_votes (user_id, reply_id, vote_id) VALUES (?,?,?)""",
            (user_id, reply.id, 2),
        )
    return new_vote


def downvote(user_id: int, reply: Reply):
    user_vote = read_query(
        """SELECT user_id, reply_id, vote_id FROM user_votes WHERE user_id=? and reply_id=?""",
        (user_id, reply.id),
    )  # checks whether the user has given an upvote for the same reply before or downvote already exists
    if user_vote and user_vote[0][2] == 2:
        new_vote = update_query(
            """UPDATE user_votes SET vote_id=? WHERE user_id=? and reply_id=? and vote_id=2""",
            (1, user_id, reply.id),
        )
    elif user_vote and user_vote[0][2] == 1:
        return user_vote
    else:
        new_vote = insert_query(
            """INSERT into user_votes (user_id, reply_id, vote_id) VALUES (?,?,?)""",
            (user_id, reply.id, 1),
        )
    return new_vote
