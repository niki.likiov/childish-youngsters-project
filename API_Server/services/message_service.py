from data import database
from data.models import CreatedMessage, Message
from services import users_service


def get_messages_from_user(
    username: str | None, authenticated_user_id: int, read_data_func=None
):
    if read_data_func is None:
        read_data_func = database.get_data_func

    recipient_id = users_service.find_by_username(username).id
    messages = read_data_func(
        """SELECT * FROM messages WHERE author_id = ? and recipient_id = ? or author_id = ? and recipient_id = ?
        ORDER BY sent_on desc""",
        (authenticated_user_id, recipient_id, recipient_id, authenticated_user_id),
    )

    return (Message.from_query_result(*message) for message in messages)


def get_correspondents(logged_user_id, read_data_func=None):
    if read_data_func is None:
        read_data_func = database.get_data_func

    message_correspondents = read_data_func(
        "SELECT distinct m.author_id, m.recipient_id, u.username, u2.username "
        "FROM messages as m "
        "JOIN users as u "
        "on m.author_id = u.id "
        "JOIN users as u2 "
        "on m.recipient_id = u2.id "
        "WHERE m.author_id = ? or m.recipient_id = ?;",
        (
            logged_user_id,
            logged_user_id,
        ),
    )

    unique_correspondents = get_unique_corrs(message_correspondents)
    return unique_correspondents


def create(
    message: CreatedMessage,
    author_id: int,
    insert_data_func=None,
    read_data_func=None,
    find_user_func=None,
):
    if find_user_func is None:
        find_user_func = users_service.find_by_username

    recipient_id = find_user_func(message.recipient_username).id

    if insert_data_func is None:
        insert_data_func = database.insert_query

    generated_id = insert_data_func(
        f"INSERT INTO messages (content, author_id, recipient_id) VALUES (?,?,?);",
        (message.content, author_id, recipient_id),
    )

    if read_data_func is None:
        read_data_func = database.get_data_func

    generated_message = read_data_func(
        f"SELECT * FROM messages WHERE id = ?",
        (generated_id,),
    )
    return Message.from_query_result(*generated_message[0])


def get_unique_corrs(corrs: list[tuple]) -> list:
    unique_cors = set()
    for line in corrs:
        if line[3] not in unique_cors:
            unique_cors.add(line[3])
        elif line[4] not in unique_cors:
            unique_cors.add(line[4])

    return list(unique_cors)
