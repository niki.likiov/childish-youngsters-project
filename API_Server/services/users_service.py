from typing import Any

from data.database import insert_query, read_query
from data.models import Role, User, Reply
from mariadb import IntegrityError
import jwt

from data.models import Category

key = "Super"

# passwords should be secured as hashstrings in DB
def _hash_password(password: str):
    from hashlib import sha256

    return sha256(password.encode("utf-8")).hexdigest()


def find_by_username(username: str, get_data_func=None) -> User | None:
    if get_data_func == None:
        get_data_func = read_query

    data = get_data_func(
        "SELECT id, name, surname, username, email, password, role, created_on, mentor_id FROM users WHERE username = ?",
        (username,),
    )
    return next((User.from_query_result(*row) for row in data), None)


def find_by_email(email: str, get_data_func=None) -> User | None:
    if get_data_func == None:
        get_data_func = read_query

    data = get_data_func(
        "SELECT id, name, surname, username, email, password, role, created_on, mentor_id FROM users WHERE email = ?",
        (email,),
    )
    return next((User.from_query_result(*row) for row in data), None)


def find_by_id(id: int, get_data_func=None) -> User | None:
    if get_data_func == None:
        get_data_func = read_query

    data = get_data_func(
        "SELECT id, name, surname, username, email, password, role, created_on, mentor_id FROM users WHERE id = ?",
        (id,),
    )
    return next((User.from_query_result(*row) for row in data), None)


def try_login_with_username(
    username: str, password: str, get_data_func=None
) -> User | None:
    if get_data_func == None:
        get_data_func = find_by_username
    user = get_data_func(username)

    password = _hash_password(password)
    return user if user and user.password == password else None


def try_login_with_email(email: str, password: str, get_data_func=None) -> User | None:
    if get_data_func == None:
        get_data_func = find_by_email
    user = get_data_func(email)

    password = _hash_password(password)
    return user if user and user.password == password else None


def create(
    name: str,
    surname: str,
    username: str,
    email: str,
    password: str,
    get_data_func=None,
) -> dict[str, User | Any] | str:
    if get_data_func == None:
        get_data_func = insert_query
    password = _hash_password(password)
    try:
        generated_id = get_data_func(
            "INSERT INTO users(name, surname, username, email, password, role) VALUES (?,?,?,?,?,?)",
            (name, surname, username, email, password, Role.MEMBER),
        )
        if generated_id:
            token = jwt.encode({generated_id: username}, key, algorithm="HS256")
        return {
            "User": User(
                id=generated_id,
                name=name,
                surname=surname,
                username=username,
                email=email,
                password=password,
                role=Role.MEMBER,
            ),
            "token": token,
        }

    except IntegrityError as err:
        error = "Error: {}".format(err)
        # mariadb raises this error when a constraint is violated
        # in that case we have duplicate usernames
        return error


def adminize(user_to_be_admin: User, get_data_func=None) -> User:
    if get_data_func == None:
        get_data_func = insert_query
    get_data_func(
        "UPDATE users SET users.role='admin' WHERE users.id = ?",
        (user_to_be_admin.id,),
    )
    return user_to_be_admin


def assign_mentor(
    user_to_be_assigned_to: User, mentor_id: int, get_data_func=None
) -> User:
    if get_data_func == None:
        get_data_func = insert_query
    get_data_func(
        "UPDATE users SET users.mentor_id=? WHERE users.id = ?",
        (
            mentor_id,
            user_to_be_assigned_to.id,
        ),
    )
    return user_to_be_assigned_to


def is_authenticated(token: str, get_data_func=None) -> bool:
    try:
        decoded_token = jwt.decode(token, key, algorithms="HS256")
        items = list(decoded_token.items())
        id = int(items[0][0])
        username = items[0][1]
        if get_data_func == None:
            get_data_func = find_by_username
        user = get_data_func(username)
        if user and user.id == id:
            return True
    except:
        jwt.InvalidSignatureError
        return False


def from_token(token: str) -> User | None:
    decoded_token = jwt.decode(token, key, algorithms="HS256")
    value = list(decoded_token.values())
    username = value[0]
    return find_by_username(username)


def get_user_posts(user_id: int) -> list[Reply]:
    pass


def owns_reply(user_id: int, reply: Reply) -> bool:
    pass
    # return reply.author_id == user.id


def privileged_by_category(category: Category, get_data_func=None) -> list[User]:
    if get_data_func == None:
        get_data_func = read_query
    data = get_data_func(
        """SELECT id,username,email,password,role from users as u 
        join users_categories as uc 
        on u.id=uc.user_id 
        where category_id=?""",
        (category.id,),
    )
    return (User.from_query_result(*row) for row in data)


def is_admin(user: User) -> bool:
    return user.role == Role.ADMIN
