from fastapi import APIRouter, Header
from common.auth import get_user_or_raise_401
from data.models import CreatedMessage
from services import message_service

messages_router = APIRouter(prefix="/messages")


@messages_router.post("/")
def send(message: CreatedMessage, x_token: str = Header()):
    author = get_user_or_raise_401(token=x_token)
    # recipient = users_service.find_by_username(message.recipient_name)

    message_generated = message_service.create(message, author.id)

    return message_generated


@messages_router.get("/")
def view_conversation(username: str, x_token: str = Header()):
    auth_user = get_user_or_raise_401(x_token)

    return message_service.get_messages_from_user(username, auth_user.id)


@messages_router.get("/users")
def view_correspondents(x_token: str = Header()):
    auth_user = get_user_or_raise_401(x_token)

    uniq_corrs = message_service.get_correspondents(auth_user.id)
    uniq_corrs.remove(auth_user.username)

    return {f"correspondents of {auth_user.username}": uniq_corrs}
