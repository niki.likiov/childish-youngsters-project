from fastapi import APIRouter, Header
from common.auth import get_user_or_raise_401
from common.responses import BadRequest, NotFound
from data.models import LoginData, RegisterData
from services import users_service, category_service


users_router = APIRouter(prefix="/users")


@users_router.post("/login")
def login(data: LoginData):
    if data.username:
        user = users_service.try_login_with_username(data.username, data.password)
    if data.email:
        user = users_service.try_login_with_email(data.email, data.password)
    if user:
        return f"User {user.username} logged in"
    else:
        return BadRequest("Invalid login data")


@users_router.get("/info")
def user_info(
    email: str | None = None,
    username: str | None = None,
    id: int | None = None,
    x_token: str = Header(),
):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        if username:
            return users_service.find_by_username(username)
        elif id:
            return users_service.find_by_id(id)
        elif email:
            return users_service.find_by_email(email)
        else:
            return NotFound


@users_router.post("/register")
def register(data: RegisterData):
    user_and_token_or_error = users_service.create(
        data.name, data.surname, data.username, data.email, data.password
    )
    if user_and_token_or_error == (
        f"Error: Duplicate entry '{data.username}' for key 'username_UNIQUE'"
    ):
        return BadRequest(
            f"Username {data.username} is taken. Please choose a different username"
        )
    if user_and_token_or_error == (
        f"Error: Duplicate entry '{data.email}' for key 'email_UNIQUE'"
    ):
        return BadRequest(
            f"Email {data.email} is taken. Please choose a different e-mail"
        )

    return user_and_token_or_error


@users_router.post("/logout")
def logout(x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    return f"User {user.username} logged out"


@users_router.put("/make_admin")
def make_admin(username: str, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        user_to_be_admin = users_service.find_by_username(username)
        if user_to_be_admin:
            if user_to_be_admin.is_admin():
                return BadRequest(f"User {username} is already admin")
            users_service.adminize(user_to_be_admin)
            return {f"User {username} is now admin"}
        else:
            return BadRequest(f"There is no such user: {username}")
    else:
        return BadRequest(f"User {user.username} cannot give admin rights")


@users_router.get("/privileged/{id}")
def get_privileged(id: int, x_token: str = Header()):

    user = get_user_or_raise_401(x_token)
    if not user.is_admin():
        return BadRequest(f"You must be admin to view this content")
    category = category_service.get_by_id(id)

    if category == None:
        return BadRequest(f"There is no category with id: {id}")
    if not category.is_it_private():
        return BadRequest(f"Category with id: {id} - {category.name} is not private")

    return users_service.privileged_by_category(category)
