from fastapi import APIRouter, Header
from common.auth import get_user_or_raise_401
from common.responses import BadRequest, NotFound
from data.models import Category, CreateCategoryData
from services import category_service, topics_service, users_service


categories_router = APIRouter(prefix="/categories")


@categories_router.get("/")
def get_categories(x_token: str | None = Header(default=None)) -> list[Category]:
    if not x_token:
        categories = category_service.all_for_guest()
    else:
        user = get_user_or_raise_401(x_token)
        if user.is_admin():
            categories = category_service.all_for_admin()
        else:
            categories = category_service.all_for_user(user.id)
    return categories


@categories_router.get("/{id}")
def get_category_by_id(
    id: int,
    sort: str | None = None,
    sort_by: str | None = None,
    search: str | None = None,
    limit: int | None = None,
    offset: int | None = None,
):
    category = category_service.get_by_id(id)

    if category is None:
        return NotFound()
    else:
        topics = topics_service.all(search, limit, offset, id)
        if sort and (sort == "asc" or sort == "desc"):
            return topics_service.sort(
                topics, reverse=sort == "desc", attribute=sort_by
            )
        return topics


@categories_router.post("/")
def create_category(data: CreateCategoryData, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        created_category_or_error = category_service.create(data.name)
        if created_category_or_error == (
            f"mariadb.IntegrityError: Duplicate entry '{data.name}' for key 'name_UNIQUE'"
        ):
            return BadRequest(f"Category name '{data.name}' already exists")

        return created_category_or_error
    else:
        return BadRequest(
            f"User {user.username} has no admin rights to create a category"
        )


@categories_router.put("/lock/{id}")
def lock_category(id: int, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        if not category_service.exists(id):
            return BadRequest(f"Category with id {id} does not exist")
        category = category_service.get_by_id(id)
        return category_service.lock(category)


@categories_router.put("/unlock/{id}")
def unlock_category(id: int, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        if not category_service.exists(id):
            return BadRequest(f"Category with id {id} does not exist")
        category = category_service.get_by_id(id)
        return category_service.unlock(category)


@categories_router.put("/make_private/{id}")
def make_category_private(id: int, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        if not category_service.exists(id):
            return BadRequest(f"Category with id {id} does not exist")
        category = category_service.get_by_id(id)
        return category_service.make_private(category)


@categories_router.put("/make_public/{id}")
def make_category_public(id: int, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        if not category_service.exists(id):
            return BadRequest(f"Category with id {id} does not exist")
        category = category_service.get_by_id(id)
        return category_service.make_public(category)


@categories_router.put("/give_access/{id}")
def give_access_to_user(
    id: int, username: str, permission: str, x_token: str = Header()
):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        if not category_service.exists(id):
            return BadRequest(f"Category with id {id} does not exist")
        category = category_service.get_by_id(id)
        user = users_service.find_by_username(username)
        if not user:
            return BadRequest(f"No user with username {username}")
        return category_service.give_access(category, user.id, permission)


@categories_router.put("/revoke_access/{id}")
def revoke_access_from_user(id: int, username: str, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        if not category_service.exists(id):
            return BadRequest(f"Category with id {id} does not exist")
        category = category_service.get_by_id(id)
        user = users_service.find_by_username(username)
        if not user:
            return BadRequest(f"No user with username {username}")
        response = category_service.revoke_access(category, user.id)
        if (
            response
            == f"User with id: {user.id} now has no permission to category {category.name}"
        ):
            return f"User {user.username} now has no permission to category {category.name}"
        else:
            return BadRequest(
                f"User {username} has no rights for category {category.name}"
            )
