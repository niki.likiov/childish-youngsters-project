from fastapi import APIRouter, Header
from fastapi.openapi.models import Response
from common.auth import get_user_or_raise_401
from common.responses import BadRequest
from data.database import insert_query
from data.models import Reply, Topic, Votes, CreatedReply
from services import replies_service, topics_service

replies_router = APIRouter(prefix="/replies")


@replies_router.get("/", response_model=list[Reply])
def get_replies():
    replies = replies_service.all()

    return replies


@replies_router.post("/new_reply/{topic_id}")
def new_reply(topic_id: int, reply: CreatedReply, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user:
        topic = topics_service.find_by_id(topic_id)
        if topic:
            created_reply = replies_service.create(reply, user.id, topic_id)
            return created_reply
        return BadRequest(f"There is no topic with id:{topic_id}")
    else:
        return BadRequest("You should be logged in to create a reply.")


@replies_router.get("/{reply_id}")
def find_by_id(reply_id: int):
    topic = replies_service.find_by_id(reply_id)

    return topic or Response(status_code=404)


@replies_router.get("/topic/{topic_id}")
def find_by_topic_id(topic_id: int):
    topic = replies_service.all_by_topic(topic_id)

    return topic or Response(status_code=404)


@replies_router.post("/upvote/{reply_id}")
def upvote(reply_id: int, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    reply = replies_service.find_by_id(reply_id)
    if user and reply:
        new_vote = replies_service.upvote(user.id, reply)
    return new_vote


@replies_router.post("/downvote/{reply_id}")
def downvote(reply_id: int, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    reply = replies_service.find_by_id(reply_id)
    if user and reply:
        new_vote = replies_service.downvote(user.id, reply)
    return new_vote


@replies_router.post("/best_reply/{reply_id}")
def best_reply(reply_id: int, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    reply = replies_service.find_by_id(reply_id)
    if user and reply:
        new_vote = replies_service.best_vote(user.id, reply)
    return new_vote
