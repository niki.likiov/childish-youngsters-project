from fastapi import APIRouter, Response, Header
from common.auth import get_user_or_raise_401
from common.responses import BadRequest, NotFound
from services import topics_service, replies_service
from data.models import Topic, CreatedTopic

topics_router = APIRouter(prefix="/topics")


@topics_router.get("/", response_model=list[Topic])
def get_topics(
    sort: str | None = None,
    sort_by: str | None = None,
    search: str | None = None,
    limit: int | None = None,
    offset: int | None = None,
):
    topics = topics_service.all(search, limit, offset, category_id=None)

    if sort and (sort == "asc" or sort == "desc"):
        return topics_service.sort(topics, reverse=sort == "desc", attribute=sort_by)
    return topics


@topics_router.get("/{id}")
def get_topic_by_id(id: int):
    topic = topics_service.find_by_id(id)

    return topic or NotFound()


@topics_router.put("/{id}")
def edit_topic(id, data):
    topic = topics_service.edit(id, data)

    return topic


@topics_router.post("/new_topic")
def new_topic(topic: CreatedTopic, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user:
        created_topic = topics_service.create(topic, user.id)

        return created_topic


@topics_router.put("/lock/{id}")
def lock_topic(id: int, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        if not topics_service.exists(id):
            return BadRequest(f"Topic with id {id} does not exist")
        topic = topics_service.find_by_id(id)
        return f"Topic {topic.id} is now locked."


@topics_router.put("/unlock/{id}")
def unlock_category(id: int, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    if user.is_admin():
        if not topics_service.exists(id):
            return BadRequest(f"Topic with id {id} does not exist")
        topic = topics_service.find_by_id(id)
        return f"Topic {topic.id} is now unlocked."


@topics_router.put("/vote/{id}")
def best_vote(id: int, x_token: str = Header()):
    user = get_user_or_raise_401(x_token)
    reply = replies_service.find_by_id(id)
    topic = topics_service.find_by_id(reply.id)

    if user.id == topic.author_id:
        return f'Best reply for "{topic.title}" is set.'
    else:
        return "You are not the author of the topic."
