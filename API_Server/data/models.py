from datetime import datetime
from pydantic import BaseModel, constr


class Category(BaseModel):
    id: int | None
    name: str
    is_private: int
    is_locked: int

    @classmethod
    def from_query_result(cls, id, name, is_private, is_locked):
        return cls(
            id=id,
            name=name,
            is_private=is_private,
            is_locked=is_locked,
        )

    def is_it_private(self):
        return self.is_private == 1


class CreateCategoryData(BaseModel):
    name: str


class Reply(BaseModel):
    id: int | None
    text: str
    author_id: int
    created_on: datetime
    topic_id: int

    @classmethod
    def from_query_result(cls, id, text, author_id, created_on, topic_id):
        return cls(
            id=id,
            text=text,
            author_id=author_id,
            created_on=created_on,
            topic_id=topic_id,
        )


class CreatedReply(BaseModel):
    text: str


class Topic(BaseModel):
    id: int | None
    title: str
    is_locked: bool
    created_on: datetime
    author_id: int
    category_id: int

    @classmethod
    def from_query_result(
        cls, id, title, is_locked, created_on, author_id, category_id
    ):
        return cls(
            id=id,
            title=title,
            is_locked=is_locked,
            created_on=created_on,
            author_id=author_id,
            category_id=category_id,
        )


class CreatedTopic(BaseModel):
    title: str
    category_id: int
    first_reply: str


class Role:
    MEMBER = "member"
    ADMIN = "admin"


TUsername = constr(regex="^\w{2,30}$")
TEmail = constr(
    regex="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}"
)  # Regex for e-mail syntax
TPassword = constr(
    regex="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
)  # Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
TWebsite = constr(
    regex="/^[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&//=]*)$/"
)


class User(BaseModel):
    id: int | None
    name: str
    surname: str
    username: TUsername
    email: TEmail
    password: str
    role: str
    created_on: datetime | None
    mentor_id: int | None

    def is_admin(self):
        return self.role == Role.ADMIN

    @classmethod
    def from_query_result(
        cls,
        id,
        name,
        surname,
        username,
        email,
        password,
        role,
        created_on,
        mentor_id,
    ):
        return cls(
            id=id,
            name=name,
            surname=surname,
            username=username,
            email=email,
            password=password,
            role=role,
            created_on=created_on,
            mentor_id=mentor_id,
        )


class LoginData(BaseModel):
    username: str | None
    email: str | None
    password: str


class RegisterData(BaseModel):
    name: str
    surname: str
    username: TUsername
    email: TEmail
    password: TPassword


class Company(BaseModel):
    id: int | None
    name: str
    UIC: int
    address: str
    town_id: int
    country_id: int
    website: TWebsite
    type_id: int
    VAT_registered: bool
    created_on: datetime

    def is_admin(self):
        return self.role == Role.ADMIN

    @classmethod
    def from_query_result(cls, id, username, email, password, role, mentor_id):
        return cls(
            id=id,
            username=username,
            email=email,
            password=password,
            role=role,
            mentor_id=mentor_id,
        )


class Country(BaseModel):
    id: int
    name: str


class Town(BaseModel):
    id: int
    name: str


class CompanyType(BaseModel):
    id: int
    name: str


class Advertisement(BaseModel):
    id: int
    category_id: int
    company_id: int
    title: str
    text: str


class Skills(BaseModel):
    id: int
    name: str


class Message(BaseModel):
    id: int | None
    content: str
    sent_on: datetime
    author_id: int
    recipient_id: int

    @classmethod
    def from_query_result(
        cls, id: int, content: str, sent_on: datetime, author_id: int, recipient_id: int
    ):
        return cls(
            id=id,
            content=content,
            sent_on=sent_on,
            author_id=author_id,
            recipient_id=recipient_id,
        )


class CreatedMessage(BaseModel):
    content: str
    recipient_username: str


class UserVotes(BaseModel):
    reply_id: int | None
    user_id: int
    vote_id: int


class Votes(BaseModel):
    id: int
    vote_type: str
